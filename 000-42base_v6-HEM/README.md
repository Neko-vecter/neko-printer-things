# 42 Stepper motor base to v6 mounting bracket

## This design in open alpha!

![42basev6](42base_v6.png)

### Printed Part
[v6-Toolhead-mount-A](STL/v6-Toolhead-mount-A.STL)

[v6-Toolhead-mount-B](STL/v6-Toolhead-mount-B.STL)

[[a]_Toolhead_Blower_Duct_Left_TV6_x1](STL/[a]_Toolhead_Blower_Duct_Left_TV6_x1.STL) (From voron v0)

[[a]_Toolhead_Blower_Duct_Right_TV6_x1](STL/[a]_Toolhead_Blower_Duct_Right_TV6_x1.STL) (From voron v0)


### non Printed Part
|Part Name|Part Number|
|-|-|
|M3x50|4|
|M3x15|2|
|M3 Nut|4|
|M3 Brass Embedded Nut Insert|2|
|3010 axial fan |1|
|3010 blower fans|2|

### Cura Print Setting
[v6-Toolhead-mount-AB](Cura-Setting/v6-Toolhead-mount-AB.3mf)
[[a]_Toolhead_Blower_Duct_LR_TV6_x1](Cura-Setting/[a]_Toolhead_Blower_Duct_LR_TV6_x1.3mf)
